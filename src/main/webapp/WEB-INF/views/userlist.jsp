<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 05/07/2019
  Time: 11:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>User List</h1>


<select size="10">
    <c:forEach items="${users}" var = "user">
        <option>${user.getName()}</option>
    </c:forEach>

<select>

    <ul>
        <c:forEach items="${users}" var = "user">

            <li><a href="userdetail/${user.getId()}">${user.getName()}</a></li>
        </c:forEach>
    </ul>




<%
        // NB: don't do this - use the jstl instead
        //out.println("finished");
%>
</body>
</html>
