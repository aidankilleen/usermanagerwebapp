package ie.eightwest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Collection;
import java.util.Locale;

@Controller
public class HomeController {

    @Autowired
    UserDao udb;

    @RequestMapping(method= RequestMethod.GET, value="/home")
    public String home(Locale locale, Model model) {

        try {
            User user = udb.getUser(2);
            model.addAttribute("user", user);

        } catch (UserDaoException e) {
            e.printStackTrace();
        }
        model.addAttribute("title", "Spring Web MVC Sample");

        //User user = new User(1, "Aidan", "aidan@gmail.com", true);

        return "home"; // this is the name of the view
    }

    @RequestMapping(method=RequestMethod.GET, value="/userlist")
    public String getUsers(Model model) {

        Collection<User> users = udb.getUsers();

        model.addAttribute("users", users);

        return "userlist";

    }

    @RequestMapping(method=RequestMethod.GET, value="/userdetail/{id}")
    public String showUser(@PathVariable long id, Model model) {

        System.out.println("showUser called:" + id);
        // get user id from the db
        try {
            User user = udb.getUser(id);
            model.addAttribute("user", user);

        } catch (UserDaoException e) {
            e.printStackTrace();
        }


        // add it to the model

        // return the view
        return "userdetail";
    }
}
