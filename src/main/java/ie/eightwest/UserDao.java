package ie.eightwest;

import java.util.Collection;

public interface UserDao {
    User addUser(User user) throws UserDaoException;
    User updateUser(User user) throws UserDaoException;
    void deleteUser(long id) throws UserDaoException;
    Collection<User> getUsers();
    User getUser(long id) throws UserDaoException;
    void close() throws UserDaoException;
}
