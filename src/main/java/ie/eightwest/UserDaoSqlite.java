package ie.eightwest;

import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;


public class UserDaoSqlite implements UserDao {

    protected Connection conn;
    protected String filePath;
    protected final String baseUrl = "jdbc:sqlite:";
    protected final String defaultFilePath = "c:/work/eightwest/users.db";

    public UserDaoSqlite(String filePath) {
        this.filePath = filePath;
        openConnection(filePath);
    }
    public UserDaoSqlite() {
        openConnection(defaultFilePath);
    }
    public void openConnection(String filePath) {
        try {
            Class.forName("org.sqlite.JDBC");
            String url = baseUrl + filePath;
            conn = DriverManager.getConnection(url);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public User addUserBad(User user) throws UserDaoException {
        try {

            Statement stmt = conn.createStatement();

            String sql = "INSERT INTO users " +
                    "(name, email, active) " +
                    "VALUES('" + user.getName() + "','" + user.getEmail() +
                    "','" + (user.isActive() ? "true":"false") + "')";

            System.out.println(sql);
            stmt.executeUpdate(sql);
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;

    }

    @Override
    public User addUser(User user) throws UserDaoException {
        try {

            String sql;
            if (user.getId() != -1) {

                sql = "INSERT INTO users " +
                        "(name, email, active, id) " +
                        "VALUES(?, ?, ?, ?)";

            } else {
                sql = "INSERT INTO users " +
                        "(name, email, active) " +
                        "VALUES(?, ?, ?)";
            }
            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, user.getName());
            pstmt.setString(2, user.getEmail());
            pstmt.setBoolean(3, user.isActive());

            if (user.getId() != -1) {
                pstmt.setLong(4, user.getId());
            }
            pstmt.executeUpdate();
            pstmt.close();

            // get the id of the inserted row
            sql = "select last_insert_rowid()";
            pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                user.setId(rs.getLong(1));
            }
            rs.close();
            pstmt.close();

        } catch (SQLException e) {
            // user id already exists
            throw new UserDaoException("User id already exists");
        }
        return user;

    }

    @Override
    public User updateUser(User user) throws UserDaoException {

        try {
            String sql = "UPDATE users SET name =?, email=?, active=? WHERE id=?";
            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, user.getName());
            pstmt.setString(2, user.getEmail());
            pstmt.setBoolean(3, user.isActive());
            pstmt.setLong(4, user.getId());

            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return null;
    }

    @Override
    public void deleteUser(long id) throws UserDaoException {

        try {
            String sql = "DELETE FROM users WHERE id = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, id);
            int recordCount = pstmt.executeUpdate();
            pstmt.close();

            if (recordCount == 0) {
                throw new UserDaoException("record not found:" + id);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    @Override
    public Collection<User> getUsers() {
        //Collection<User> users = new Collection<User>();
        ArrayList<User> users = new ArrayList<User>();
        try {
            String sql = "SELECT * FROM users";
            PreparedStatement pstmt = conn.prepareStatement(sql);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                long id = rs.getLong("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                boolean active = rs.getBoolean("active");

                User user = new User(id, name, email, active);

                users.add(user);
            }
            rs.close();
            pstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public User getUser(long id) throws UserDaoException {
        
        User user = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, id);

            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                user = new User(rs.getLong("id"),
                                rs.getString("name"),
                                rs.getString("email"),
                                rs.getBoolean("active"));
            } else {
                throw new UserDaoException("User not found:" + id);
            }
            rs.close();
            pstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public void close() throws UserDaoException {
        try {
            conn.close();
        } catch (SQLException e) {
            throw new UserDaoException("Database closing error");
        }
    }
}
