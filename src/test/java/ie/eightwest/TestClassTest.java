package ie.eightwest;

import org.junit.Test;

import static org.junit.Assert.*;

public class TestClassTest {

    @Test
    public void testGetName() {
        assertEquals(TestClass.getName(), "Aidan");
    }

    @Test
    public void testStaticAccess() {
        assertEquals(TestClass.name, "Aidan");
    }

    @Test
    public void testCreation() {
        TestClass tc = new TestClass();

        assertNotNull(tc);

    }

}